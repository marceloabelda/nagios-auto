#!/bin/bash


##########################################################################################################################

# Copyright 2011 Yogesh Panchal (yspanchal@gmail.com)
# Author: Yogesh Panchal (yspanchal@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# File : Nagios-Auto.sh : A simple shell script to Install Nagios Monitoring Tool.

##########################################################################################################################


# Text color variables

txtund=$(tput sgr 0 1)    # Underline
txtbld=$(tput bold)       # Bold
txtred=$(tput setaf 1)    # Red
txtgrn=$(tput setaf 2)    # Green
txtylw=$(tput setaf 3)    # Yellow
txtblu=$(tput setaf 4)    # Blue
txtpur=$(tput setaf 5)    # Purple
txtcyn=$(tput setaf 6)    # Cyan
txtwht=$(tput setaf 7)    # White
txtrst=$(tput sgr0)       # Text reset

#########################################################################################################################
echo ""
echo ""
echo "#############################################################"
echo "#############################################################"
echo "##                                                         ##"
echo "##${txtgrn}       Welcome to Nagios Auto Installation Script${txtrst}        ##"
echo "##           Original Written By Yogesh Panchal            ##"
echo "##                  Revision By ngoprek                    ##"
echo "##           ${txtred}  yogesh.pachal@fafadiatech.com${txtrst}               ##"
echo "##          ${txtred}        ngoprek@ngoprek.biz   ${txtrst}                 ##"
echo "##                                                         ##"
echo "#############################################################"
echo "#############################################################"
echo ""
echo ""
#############################################################################################################################



sleep 3

############ Variable Definitions  ################
log=/tmp/nagios_setup.log
nagios_core=`echo $latest_nagios`
folder_nagios=`echo $nagios_core | sed -e 's/.tar.gz//g'`
nagios_plugin=`echo $latest_plugin`
folder_plugin=`echo $nagios_plugin | sed -e 's/.tar.gz//g'`

############ Functions Definition ################

stop() {
sleep 2
echo ""
echo "${txtred}****** Nagios Installation Aborted *******${txtrst}"
echo ""
echo "${txtred}Error${txtrst}"
echo "****** Nagios Installation Aborted *******" >> $log
echo "" >> $log
echo "Error" $log
exit 0
}

finish() {
echo "${txtgrn}Nagios Installation Completed successfully${txtrst}"
echo "Nagios Installation Completed successfully" >> $log
echo ""
echo "${txtgrn}Following are the details:${txtrst}"
echo "Following are the details:" >> $log
echo ""
echo "${txtgrn}Nagios Installation Directory ' ${txtred}/usr/local/nagios/${txtrst} '${txtrst}"
echo "Nagios Installation Directory ' /usr/local/nagios/ '" >> $log
echo ""
echo "${txtgrn}Nagios Main Configuration File ' ${txtred}/usr/local/nagios/etc/nagios.cfg${txtrst} '${txtrst}"
echo "Nagios Main Configuration File ' /usr/local/nagios/etc/nagios.cfg '" >> $log
echo ""
echo "${txtgrn}Nagios Object configuration Files ' ${txtred}/usr/local/nagios/etc/objects/${txtrst} '${txtrst}"
echo "Nagios Object configuration Files ' /usr/local/nagios/etc/objects/ '" >> $log
echo ""
echo "${txtgrn}Nagios Apache Configuration File ' ${txtred}/etc/httpd/conf.d/nagios.conf${txtrst} '${txtrst}"
echo "Nagios Apache Configuration File ' /etc/httpd/conf.d/nagios.conf '" >> $log
echo ""
echo "${txtgrn}Nagios Web Frontend Url ' ${txtred}http://localhost/nagios${txtrst} ' or ' ${txtred}http://yourhost/nagios${txtrst} '${txtrst}"
echo "Nagios Web Frontend Url ' http://localhost/nagios ' or ' http://your_host/nagios '" >> $log
echo ""
echo "${txtgrn}Nagios Web Frontend Username : ${txtred}nagiosadmin${txtrst}${txtrst}"
echo "Nagios Web Frontend Username : nagiosadmin" >> $log
echo ""
echo "${txtgrn}Nagios Web Frontend Password : ${txtred}***********${txtrst}${txtrst}"
echo "Nagios Web Frontend Password : ***********" >> $log
echo
echo "${txtred} ********                  Please change Contacts in               ******** ${txtrst}"
echo "${txtred} ********         /usr/local/nagios/etc/objects/contacts.cfg       ******** ${txtrst}"
echo ""
echo ""
echo "${txtred} ******** Check Nagios installation Log File /tmp/nagios_setup.log ******** ${txtrst}"
echo ""
echo " ${txtgrn}********** Thank You For Using Nagios Auto Installation Script ********** ${txtrst}"
echo ""
echo "                  ${txtred}******** ngoprek@ngoprek.biz ******** ${txtrst}"
echo "                         ${txtred}****** Thank You ****** ${txtrst}"
echo " ********** Thank You For Using Nagios Auto Installation Script ********** " >> $log
echo "" >> $log
echo "                  ******** ngoprek@ngoprek.biz ******** " >> $log
cd ..
rm -rf index* *.txt
exit 0
sleep 2
}

check() {
if [ $? != 0 ]
then
echo
echo "${txtred}Nagios installation stopped because there was a problem${txtrst}"
stop
fi
}

nagiosplugin_centos_update() {
echo "${txtylw}Extract package Nagios plugin${txtrst}"
sleep 2
tar -zxvf $nagios_plugin;check
echo "Extract package Nagios plugin successfull" >> $log
echo
echo "${txtylw}Compile Nagios plugins update version${txtrst}"
sleep 2
cd $folder_plugin
./configure --with-nagios-user=nagios --with-nagios-group=nagios;check;make;check;make install;check
echo "Compile Nagios plugins update version successfull" >> $log
echo "${txtylw}Adding Nagios to the list of system services and have it automatically start when the system boots${txtrst}"
sleep 1
echo ""
systemctl enable nagios 2>/dev/null
systemctl enable httpd 2>/dev/null
chkconfig --add nagios 2>/dev/null
chkconfig --level 345 nagios on 2>/dev/null
chkconfig --add httpd 2>/dev/null
chkconfig --level 345 httpd on 2>/dev/null
echo "Adding Nagios to the list of system services successfull" >> $log
echo "${txtylw}Verify the sample Nagios configuration files${txtrst}"
sleep 1
echo ""
echo ""
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg;check;
echo "Verify the sample Nagios configuration files successfull" >> $log
echo
echo
echo "${txtylw}Restarting Nagios Service${txtrst}"
sleep 1
echo ""
echo ""
systemctl restart nagios.service 2>/dev/null
service nagios restart 2>/dev/null
echo "Restarting Nagios Service successfull" >> $log
echo
finish
}

nagiosplugin_221() {
echo "${txtylw}Extract package Nagios plugins${txtrst}"
sleep 2
tar -zxvf nagios-plugins-2.2.1.tar.gz;check
echo "Extract package Nagios plugins successfull" >> $log
echo
echo
echo "${txtylw}Compile Nagios plugins version 2.2.1${txtrst}"
sleep 2
cd nagios-plugins-2.2.1;
./configure --with-nagios-user=nagios --with-nagios-group=nagios;check;make;check;make install;check;
echo "Compile Nagios plugins version 2.2.1 successfull" >> $log
echo
echo
echo "${txtylw}Adding Nagios to the list of system services and have it automatically start when the system boots ${txtrst}"
echo
sleep 2
systemctl enable nagios 2>/dev/null
systemctl enable httpd 2>/dev/null
chkconfig --add nagios 2>/dev/null
chkconfig --level 345 nagios on 2>/dev/null
chkconfig --add httpd 2>/dev/null
chkconfig --level 345 httpd on 2>/dev/null
echo "Adding Nagios to the list of system services successfull" >> $log
echo
echo
echo "${txtylw}Verify the sample Nagios configuration files${txtrst}"
sleep 1
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg;check;
echo "Verify the sample Nagios configuration files successfull" >> $log
echo ""
echo "${txtylw}Restarting Nagios Service${txtrst}"
sleep 1
echo ""
systemctl restart nagios.service 2>/dev/null
service nagios restart 2>/dev/null
echo "Restarting Nagios Service successfull" >> $log
echo
finish
}


nagioscore_centos_update() {
echo "${txtylw}Extract package Nagios${txtrst}"
sleep 2
tar -zxvf $nagios_core
check
echo "Extract package Nagios successfull" >> $log
echo;echo
echo "${txtylw}Adding user and group for nagios${txtrst}"
echo;echo
sleep 2
useradd -m nagios;groupadd nagcmd;usermod -a -G nagcmd nagios;usermod -a -G nagcmd apache
echo "Adding user and group for nagios successfull" >> $log
echo;echo
echo "${txtylw}Compiling Nagios${txtrst}"
sleep 2
cd $folder_nagios;./configure --with-command-group=nagcmd;check;make all;check;make install;check;make install-init;check;make install-config;check;make install-commandmode;check;make install-webconf;check;
echo "Compiling Nagios successfull" >> $log
echo;echo
sleep 2
echo "${txtylw}Creating a NAGIOSADMIN account for logging into the Nagios web interface.${txtrst}"
echo "${txtylw}Please enter your password below & remember the password you assign to this account you will need it later.${txtrst}"
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin;check;
echo "Creating a NAGIOSADMIN account Successfull " >> $log
echo;echo
echo "${txtylw}Restarting Apache Service${txtrst}"
sleep 1
systemctl restart httpd 2>/dev/null
service httpd restart 2>/dev/null
echo "Restarting Apache Service Successfull" >> $log
echo "${txtgrn}Nagios core update version has been installed ...!${txtrst}"
sleep 2
echo "Nagios core update version has been installed" >> $log
}


nagioscore_centos_434() {
echo "${txtylw}Extract package Nagios${txtrst}"
sleep 2
tar -zxvf nagios-4.3.4.tar.gz;check
echo "Extract package Nagios successfull" >> $log
echo;echo
echo "${txtylw}Adding user and group for nagios${txtrst}"
sleep 2
useradd -m nagios;groupadd nagcmd;usermod -a -G nagcmd nagios;usermod -a -G nagcmd apache
echo "Adding user and group for nagios successfull" >> $log
echo;echo
echo "${txtylw}Compiling Nagios${txtrst}"
sleep 2
cd nagios-4.3.4;./configure --with-command-group=nagcmd;check;make all;check;make install;check;make install-init;check;make install-config;check;make install-commandmode;check;make install-webconf;check;
echo;echo
echo "Compiling Nagios successfull" >> $log
sleep 2
echo "${txtylw}Creating a NAGIOSADMIN account for logging into the Nagios web interface.${txtrst}"
echo "${txtylw}Please enter your password below & remember the password you assign to this account you will need it later.${txtrst}"
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin;check;
echo;echo
echo "Creating a NAGIOSADMIN account successfull" >> $log
echo "${txtylw}Restarting Apache Service${txtrst}"
sleep 1
systemctl restart httpd 2>/dev/null
echo "Restarting Apache Service successfull" >> $log
echo;echo
echo "${txtgrn}Congratulation, Nagios version 4.3.4 has been installed.${txtrst}"
sleep 2
echo "Nagios core version 4.3.4 has been installed" >> $log
}

nagioscore_suse_update() {
echo "${txtylw}Extract package Nagios${txtrst}"
sleep 2
tar -zxvf $nagios_core
check
echo "Extract package Nagios successfull" >> $log
echo;echo
echo "${txtylw}Adding user and group for nagios${txtrst}"
sleep 2
a2enmod php7;useradd nagios;groupadd nagios;/usr/sbin/usermod -G nagios nagios;groupadd nagcmd;usermod -a -G nagcmd nagios;usermod -a -G nagcmd root
echo "Adding user and group for nagios successfull" >> $log
echo;echo
echo "${txtylw}Compiling Nagios${txtrst}"
sleep 2
cd nagios-4.3.4;./configure --with-command-group=nagcmd;check;make all;check;make install;check;make install-init;check;make install-config;check;make install-commandmode;check;make install-webconf;check;
echo;echo
echo "Compiling Nagios successfull" >> $log
sleep 2
echo "${txtylw}Configure Apache and Nagios${txtrst}"
sleep 2
cp -R contrib/eventhandlers/ /usr/local/nagios/libexec/;chown -R nagios:nagcmd /usr/local/nagios/libexec/eventhandlers
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
/usr/bin/install -c -m 644 sample-config/httpd.conf /etc/apache2/conf.d/nagios.conf
a2enmod version 2>/dev/null
/usr/local/nagios/bin/nagios -d /usr/local/nagios/etc/nagios.cfg
echo;echo
echo "Configure Apache successfull" >> $log
echo "${txtylw}Creating a NAGIOSADMIN account for logging into the Nagios web interface.${txtrst}"
echo "${txtylw}Please enter your password below & remember the password you assign to this account you will need it later.${txtrst}"
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin;check;
echo;echo
echo "Creating a NAGIOSADMIN account successfull" >> $log
echo "${txtylw}Restarting Apache Service${txtrst}"
sleep 1
systemctl enable apache2 2>/dev/null
systemctl start apache2 2>/dev/null
echo "Restarting Apache Service successfull" >> $log
echo;echo
echo "${txtgrn}Congratulation, Nagios version 4.3.4 has been installed${txtrst}"
sleep 2
echo "Nagios core version 4.3.4 has been installed" >> $log
}

nagioscore_suse_434() {
echo "${txtylw}Extract package Nagios${txtrst}"
sleep 2
tar -zxvf nagios-4.3.4.tar.gz
check
echo "Extract package Nagios successfull" >> $log
echo;echo
echo "${txtylw}Adding user and group for nagios${txtrst}"
sleep 2
a2enmod php7;useradd nagios;groupadd nagios;/usr/sbin/usermod -G nagios nagios;groupadd nagcmd;usermod -a -G nagcmd nagios;usermod -a -G nagcmd root
echo "Adding user and group for nagios successfull" >> $log
echo;echo
echo "${txtylw}Compiling Nagios${txtrst}"
sleep 2
cd nagios-4.3.4;./configure --with-command-group=nagcmd;check;make all;check;make install;check;make install-init;check;make install-config;check;make install-commandmode;check;make install-webconf;check;
echo;echo
echo "Compiling Nagios successfull" >> $log
sleep 2
echo "${txtylw}Configure Apache and Nagios${txtrst}"
sleep 2
cp -R contrib/eventhandlers/ /usr/local/nagios/libexec/;chown -R nagios:nagcmd /usr/local/nagios/libexec/eventhandlers
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
/usr/bin/install -c -m 644 sample-config/httpd.conf /etc/apache2/conf.d/nagios.conf
a2enmod version 2>/dev/null
/usr/local/nagios/bin/nagios -d /usr/local/nagios/etc/nagios.cfg
echo;echo
echo "Configure Apache successfull" >> $log
echo "${txtylw}Creating a NAGIOSADMIN account for logging into the Nagios web interface.${txtrst}"
echo "${txtylw}Please enter your password below & remember the password you assign to this account you will need it later.${txtrst}"
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin;check;
echo;echo
echo "Creating a NAGIOSADMIN account successfull" >> $log
echo "${txtylw}Restarting Apache Service${txtrst}"
sleep 1
systemctl enable apache2 2>/dev/null
systemctl start apache2 2>/dev/null
echo "Restarting Apache Service successfull" >> $log
echo;echo
echo "${txtgrn}Congratulation, Nagios version 4.3.4 has been installed${txtrst}"
sleep 2
echo "Nagios core version 4.3.4 has been installed" >> $log
}

nagiosplugin_suse_update() {
echo "${txtylw}Extract package Nagios plugin${txtrst}"
sleep 2
tar -zxvf $nagios_plugin
check
echo "Extract package Nagios plugin successfull" >> $log
echo
echo "${txtylw}Compile Nagios plugins update version${txtrst}"
sleep 2
cd $folder_plugin
./configure --with-nagios-user=nagios --with-nagios-group=nagios;check;
echo "Compile Nagios plugins update version successfull" >> $log
echo "${txtylw}Adding Nagios to the list of system services and have it automatically start when the system boots${txtrst}"
sleep 1
echo ""
systemctl enable /etc/systemd/system/nagios.service
echo "Adding Nagios to the list of system services successfull" >> $log
echo "${txtylw}Verify the sample Nagios configuration files${txtrst}"
sleep 1
echo ""
echo ""
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg;check;
echo "Verify the sample Nagios configuration files successfull" >> $log
echo
echo
echo "${txtylw}Restarting Nagios Service${txtrst}"
sleep 2
echo [Unit] > /etc/systemd/system/nagios.service
echo Description=Nagios >> /etc/systemd/system/nagios.service
echo BindTo=network.target  >> /etc/systemd/system/nagios.service
echo  >> /etc/systemd/system/nagios.service
echo [Install]  >> /etc/systemd/system/nagios.service
echo WantedBy=multi-user.target  >> /etc/systemd/system/nagios.service
echo  >> /etc/systemd/system/nagios.service
echo [Service]  >> /etc/systemd/system/nagios.service
echo User=nagios  >> /etc/systemd/system/nagios.service
echo Group=nagios  >> /etc/systemd/system/nagios.service
echo Type=simple  >> /etc/systemd/system/nagios.service
echo ExecStart=/usr/local/nagios/bin/nagios /usr/local/nagios/etc/nagios.cfg  >> /etc/systemd/system/nagios.service
echo ""
echo ""
systemctl start nagios 2>/dev/null
systemctl daemon-reload 2>/dev/null
echo "Restarting Nagios Service successfull" >> $log
echo
finish
}

nagiosplugin_suse_221() {
echo "${txtylw}Extract package Nagios plugins${txtrst}"
sleep 2
tar -zxvf nagios-plugins-2.2.1.tar.gz
check
echo "Extract package Nagios plugin successfull" >> $log
echo
echo "${txtylw}Compile Nagios plugins update version${txtrst}"
sleep 2
cd nagios-plugins-2.2.1;
./configure --with-nagios-user=nagios --with-nagios-group=nagios;check;
echo "Compile Nagios plugins update version successfull" >> $log
echo "${txtylw}Adding Nagios to the list of system services and have it automatically start when the system boots${txtrst}"
sleep 1
echo ""
systemctl enable /etc/systemd/system/nagios.service
echo "Adding Nagios to the list of system services successfull" >> $log
echo "${txtylw}Verify the sample Nagios configuration files${txtrst}"
sleep 1
echo ""
echo ""
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg;check;
echo "Verify the sample Nagios configuration files successfull" >> $log
echo
echo
echo "${txtylw}Restarting Nagios Service${txtrst}"
sleep 2
echo [Unit] > /etc/systemd/system/nagios.service
echo Description=Nagios >> /etc/systemd/system/nagios.service
echo BindTo=network.target  >> /etc/systemd/system/nagios.service
echo  >> /etc/systemd/system/nagios.service
echo [Install]  >> /etc/systemd/system/nagios.service
echo WantedBy=multi-user.target  >> /etc/systemd/system/nagios.service
echo  >> /etc/systemd/system/nagios.service
echo [Service]  >> /etc/systemd/system/nagios.service
echo User=nagios  >> /etc/systemd/system/nagios.service
echo Group=nagios  >> /etc/systemd/system/nagios.service
echo Type=simple  >> /etc/systemd/system/nagios.service
echo ExecStart=/usr/local/nagios/bin/nagios /usr/local/nagios/etc/nagios.cfg  >> /etc/systemd/system/nagios.service
echo ""
echo ""
systemctl start nagios 2>/dev/null
systemctl daemon-reload 2>/dev/null
echo "Restarting Nagios Service successfull" >> $log
echo
finish
}


nagioscore_ubuntu() {
echo "${txtylw}Adding user and group for nagios${txtrst}"
sleep 2
useradd nagios;groupadd nagcmd;usermod -a -G nagcmd nagios;usermod -a -G nagcmd www-data
echo "Adding user and group for nagios successfull" >> $log
echo "${txtylw}Compiling Nagios${txtrst}"
sleep 2
cd ${folder_nagios}/;./configure --with-nagios-group=nagios --with-command-group=nagcmd --with-httpd_conf=/etc/apache2/sites-enabled/;check;
make all;check;make install;check;make install-init;check;make install-config;check;make install-commandmode;check;make install-webconf;check;
echo;echo
echo "Compiling Nagios successfull" >> $log
echo;echo
echo "${txtylw}Creating a NAGIOSADMIN account for logging into the Nagios web interface.${txtrst}"
echo "${txtylw}Please enter your password below & remember the password you assign to this account you will need it later.${txtrst}"
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin;check;
echo "Creating a NAGIOSADMIN account successfull" >> $log
echo;echo
echo "${txtylw}Restarting Apache Service${txtrst}"
sleep 1
a2enmod cgi 2>/dev/null
service apache2 restart 2>/dev/null
echo "Restarting Apache Service successfull" >> $log
echo;echo
echo "${txtgrn}Congratulation, Nagios version 4.3.4 has been installed${txtrst}"

sleep 2
echo "Congratulation, Nagios version 4.3.4 has been installed." >> $log
}

nagioscore_ubuntu_update() {
echo "${txtylw}Extract package Nagios${txtrst}"
sleep 2
tar -zxvf $nagios_core;check;
echo "Extract package Nagios successfull" >> $log
echo;echo
nagioscore_ubuntu
}

nagioscore_ubuntu_434() {
echo "${txtylw}Extract package Nagios${txtrst}"
sleep 2
tar -zxvf nagios-4.3.4.tar.gz;check;
echo "Extract package Nagios successfull" >> $log
echo;echo
nagioscore_ubuntu
}

nagiosplugin_ubuntu() {
echo;echo
echo "${txtylw}Compiling Nagios plugins${txtrst}"
sleep 2
cd nagios-plugins-2.2.1/
./configure --with-nagios-user=nagios --with-nagios-group=nagios;check;make;check;make install;check;
echo;echo
echo "Compiling Nagios plugins successfull" >> $log
echo
echo
echo "${txtylw}Verify the sample Nagios configuration files${txtrst}"
sleep 2
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
echo "Verify the sample Nagios configuration files successfull" >> $log
echo [Unit] > /etc/systemd/system/nagios.service
echo Description=Nagios >> /etc/systemd/system/nagios.service
echo BindTo=network.target  >> /etc/systemd/system/nagios.service
echo  >> /etc/systemd/system/nagios.service
echo [Install]  >> /etc/systemd/system/nagios.service
echo WantedBy=multi-user.target  >> /etc/systemd/system/nagios.service
echo  >> /etc/systemd/system/nagios.service
echo [Service]  >> /etc/systemd/system/nagios.service
echo User=nagios  >> /etc/systemd/system/nagios.service
echo Group=nagios  >> /etc/systemd/system/nagios.service
echo Type=simple  >> /etc/systemd/system/nagios.service
echo ExecStart=/usr/local/nagios/bin/nagios /usr/local/nagios/etc/nagios.cfg  >> /etc/systemd/system/nagios.service
echo
echo
echo "${txtylw}Starting Nagios Service${txtrst}"
sleep 1
systemctl enable /etc/systemd/system/nagios.service
systemctl start nagios
echo "Starting Nagios Service successfull" >> $log
finish
}

nagiosplugin_ubuntu_update() {
echo "${txtylw}Extract package Nagios plugins${txtrst}"
sleep 2
tar -zxvf $nagios_plugin;check;
echo "Extract package Nagios plugin successfull" >> $log
nagiosplugin_ubuntu
}


nagiosplugin_ubuntu_221() {
echo "${txtylw}Extract package Nagios plugins${txtrst}"
sleep 2
tar -zxvf nagios-plugins-2.2.1.tar.gz;check;
echo "Extract package Nagios plugin successfull" >> $log
nagiosplugin_ubuntu
}



install_suse() {
############ Installation Some Packages Needed to Install Nagios  ################

echo "${txtylw}Installation Some Packages Needed to Install Nagios.${txtrst}"
rpm -qa| grep php > php.txt
list=`ls -l | grep php.txt | awk '{print $5}'`
#echo $list

if [ $list == 0 ]
then
sleep 1
zypper --non-interactive install autoconf gcc glibc make wget unzip gd gd-devel libmcrypt-devel libopenssl-devel gettext gettext-runtime automake net-snmp perl-Net-SNMP httpd php7 php7-gd gcc glibc vim
else
sleep 1
zypper --non-interactive install autoconf gcc glibc make wget unzip gd gd-devel libmcrypt-devel libopenssl-devel gettext gettext-runtime automake net-snmp perl-Net-SNMP httpd gcc glibc vim
fi
echo ""
nagios_suse
}

nagios_suse() {
############ Search latest version of Nagios Core  ################

echo "${txtylw}Search latest version of Nagios Core${txtrst}"
echo "${txtylw}Please wait a minute ...${txtrst}"
sleep 2
echo
wget --no-check-certificate -q https://www.nagios.org/downloads/nagios-core/thanks/?t=1504034794


############ if fails to find the last version, download version 4.3.4 ################
latest_nagios=`cat index.html* 2>/dev/null  | grep "assets.nagios.com/downloads/nagioscore/releases" | sed -n '1p' | awk '{print $2}' | sed -e 's/<[^()]*>//g' | sed -e 's/\/[^()]*\///g' | sed -e 's/\"[^()]*\">//g' | sed -e 's/href=//g' `

count=`ls -1 index.html* 2>/dev/null | wc -l `
if [ $count != 0 ]
then
        echo "${txtcyn}Latest version of Nagios core is `echo $latest_nagios` version${txtrst}"
        sleep 3
        rm -rf index.html*
        echo
        nagios_core=`echo $latest_nagios`
        folder_nagios=`echo $nagios_core | sed -e 's/.tar.gz//g'`
        echo "Downloading Nagios-Latest-version" >> $log
        sleep 2
        wget  --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/$nagios_core
        count=`ls -1 nagios* 2>/dev/null | wc -l `
        if [ $count != 0 ]
                then
                        nagioscore_suse_update
                else
                         echo ${txtred}The script detects there is a problem when download latest Nagios core version.${txtrst}
                         echo ${txtred}So, the script can not install latest Nagios version.${txtrst}
                         echo ${txtred}The Script will install Nagios core version 4.3.4.${txtrst}

                         while true
                         do
                         echo
                         read -p "Are you sure you want to continue? <y/n> " answer
                         case $answer in
                                [yY]* ) echo "${txtpur}Okay, We will download Nagios version 4.3.4${txtrst}"
                                        sleep 2
                                                                                echo
                                                                                wget  --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.4.tar.gz
                                        count=`ls -1 nagios-* 2>/dev/null | wc -l `
                                        if [ $count != 0 ]
                                                then
                                                        nagioscore_suse_434
                                                else
                                                        echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                                        wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-4.3.4.tar.gz
                                                        echo "${txtgrn}Nagios version 4.3.4 has been download and we will install it to your server${txtrst}"
                                                        sleep 1
                                                        nagioscore_suse_434
                                                fi
                                break;;
                                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                                        stop
                                break;;
                         * )     echo "Just enter Y or N, please.";;
                         esac
                         done
        fi
else
        echo ${txtred}The script detects there is a problem when the script search latest Nagios core version.${txtrst}
        echo ${txtred}So, the script can not install latest Nagios version.${txtrst}
        echo ${txtred}The Script will install Nagios core version 4.3.4${txtrst}

        while true
        do
        echo
        read -p "Are you sure you want to continue? <y/n> " answer
        case $answer in
                [yY]* ) echo "${txtpur}Okay, We will download Nagios version 4.3.4${txtrst}"
                        sleep 2
                                                echo
                        wget  --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.4.tar.gz
                        count=`ls -1 nagios-*.tar.gz  2>/dev/null | wc -l`
                        if [ $count != 0 ]
                        then
                                 nagioscore_suse_434
                        else
                                 echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                 wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-4.3.4.tar.gz
                                 echo "${txtgrn}Nagios version 4.3.4 has been download and we will install it to your server${txtrst}"
                                 sleep 1
                                 nagioscore_suse_434
                        fi
                break;;

                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                        stop
                break;;
                * )     echo "Just enter Y or N, please.";;
        esac
done
fi


############ Search latest version of Nagios Plugin  ################
echo
echo "${txtylw}We will install Nagios plugin.${txtrst}"
echo "${txtylw}Search latest version of Nagios Plugin.${txtrst}"
echo "${txtylw}Please wait a minute ...${txtrst}"
sleep 1
wget -q --no-check-certificate https://nagios-plugins.org/download/
sed  '/.sha1/d' index.html 2> /dev/null | awk '{print $7}' |  sort | cut -c15-25 > nagplug.txt
echo
year=`date +%Y`
echo
cat nagplug.txt | grep $year > latest_year.txt 2> /dev/null

for i in `cat latest_year.txt`
do
date -d $i '+%d-%m-%Y' 2> /dev/null
done > result.txt

cat result.txt | sort -t"-" -k2,2 | sed -n '$p' > latest_plugin.txt 2> /dev/null
sed  '/.sha1/d' index.html > index_latest.html 2> /dev/null
version=`cat latest_plugin.txt `
echo $version | awk '{split($1,a,"-");$1=a[3]"-"a[2]"-"a[1]}1' > reverse.txt
reverse=`cat reverse.txt`
date -d "$reverse" +"%Y-%b-%d" > plugin.txt
plugin=`cat plugin.txt`
echo "$plugin" | awk '{split($1,a,"-");$1=a[3]"-"a[2]"-"a[1]}1' > plugin1.txt
plugin1=`cat plugin1.txt`

latest_plugin=`cat index_latest.html | grep $plugin1 | awk '{print $6}' | sed 's/<.*//' | sed 's/.*>//'` 2> /dev/null
#echo $latest_plugin
sleep 3

############ if fails to find the last version, download version 2.2.1 ################
nagios_plugin=`echo $latest_plugin`
folder_plugin=`echo $nagios_plugin | sed -e 's/.tar.gz//g'` 2> /dev/null
wget --no-check-certificate https://nagios-plugins.org/download/$nagios_plugin 2> /dev/null
echo

count=`ls -1 nagios-plugin*.tar.gz  2>/dev/null | wc -l`
if [ $count != 0 ]
then
        echo "${txtcyn}Latest version of Nagios plugin is `echo $latest_plugin` version${txtrst}"
        sleep 3
        echo
        nagiosplugin_suse_update

else
        echo ${txtred}The script detects there is problem when the script search latest Nagios plugin version${txtrst}
        echo ${txtred}So, the script can not install latest Nagios plugin version${txtrst}
        echo ${txtred}The Script will install Nagios plugin version 2.2.1${txtrst}

        while true
        do
        echo
        read -p "Are you sure you want to continue? <y/n> " answer
        case $answer in
                [yY]* ) echo "${txtpur}Okay, We will install Nagios plugin version 2.2.1${txtrst}"
                        sleep 2
                        wget --no-check-certificate https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
                        count=`ls -1 nagios-plugin*.tar.gz  2>/dev/null | wc -l`
                        if [ $count != 0 ]
                        then
                                echo "${txtylw}We will install Nagios plugin version 2.2.1 to your server${txtrst}"
                                sleep 2
                                nagiosplugin_suse_221
                        else
                                echo "${txtylw}Wait a minute, we will look for another source...${txtrst}"
                                wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-plugins-2.2.1.tar.gz
                                echo "${txtgrn}Nagios Plugin version 2.2.1 has been download and we will install it to your server${txtrst}"
                                sleep 2
                                nagiosplugin_suse_221
                        fi
                break;;

                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                        stop
                break;;
                * )     echo "Just enter Y or N, please.";;
        esac
done
fi
}


install_ubuntu() {
############ Installation Some Packages Needed to Install Nagios  ################

echo "${txtylw}Installation Some Packages Needed to Install Nagios${txtrst}"
apt-get update
dpkg --get-selections | grep php > php.txt

list=`ls -l | grep php.txt | awk '{print $5}'`
#echo $list

if [ $list -eq 0 ]
then
apt-get --yes install build-essential apache2 php7.0 openssl perl make php7.0-gd libgd2-xpm-dev libapache2-mod-php7.0 libperl-dev libssl-dev daemon wget apache2-utils unzip
else
apt-get --yes install build-essential apache2 openssl perl make  libgd2-xpm-dev libperl-dev libssl-dev daemon wget apache2-utils unzip
fi
echo ""
nagios_ubuntu
}


nagios_ubuntu() {
############ Search latest version of Nagios Core  ################

echo "${txtylw}Search latest version of Nagios Core${txtrst}"
echo "${txtylw}Please wait a minute ...${txtrst}"
sleep 2
echo
wget --no-check-certificate -q https://www.nagios.org/downloads/nagios-core/thanks/?t=1504034794


############ if fails to find the last version, download version 4.3.4 ################
latest_nagios=`cat index.html* 2>/dev/null  | grep "assets.nagios.com/downloads/nagioscore/releases" | sed -n '1p' | awk '{print $2}' | sed -e 's/<[^()]*>//g' | sed -e 's/\/[^()]*\///g' | sed -e 's/\"[^()]*\">//g' | sed -e 's/href=//g' `

count=`ls -1 index.html* 2>/dev/null | wc -l `
if [ $count != 0 ]
then
        echo "${txtcyn}Latest version of Nagios core is `echo $latest_nagios` version${txtrst}"
        sleep 3
        echo
        nagios_core=`echo $latest_nagios`
        folder_nagios=`echo $nagios_core | sed -e 's/.tar.gz//g'`
        echo "Downloading Nagios-Latest-version" >> $log
        sleep 2
        wget  --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/$nagios_core
        rm -rf index.html*
                count=`ls -1 nagios* 2>/dev/null | wc -l `
        if [ $count != 0 ]
                then
                        nagioscore_ubuntu_update
                else
                         echo ${txtred}The script detects there is a problem when download latest Nagios core version.${txtrst}
                         echo ${txtred}So, the script can not install latest Nagios version.${txtrst}
                         echo ${txtred}The Script will install Nagios core version 4.3.4${txtrst}

                         while true
                         do
                         echo
                         read -p "Are you sure you want to continue? <y/n> " answer
                         case $answer in
                                [yY]* ) echo "${txtpur}Okay, We will download Nagios version 4.3.4${txtrst}"
                                        sleep 2
                                                                                echo
                                                                                wget --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.4.tar.gz
                                                                                count=`ls -1 nagios-* 2>/dev/null | wc -l `
                                        if [ $count != 0 ]
                                                then
                                                        nagioscore_ubuntu_434
                                                else
                                                        echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                                        wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-4.3.4.tar.gz
                                                        echo "${txtgrn}Nagios version 4.3.4 has been download and we will install it to your server${txtrst}"
                                                        sleep 1
                                                                                                                nagioscore_ubuntu_434
                                                fi
                                break;;
                                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                                        stop
                                break;;
                         * )     echo "Just enter Y or N, please.";;
                         esac
                         done
        fi
else
        echo ${txtred}The script detects there is a problem when the script search latest Nagios core version.${txtrst}
        echo ${txtred}So, the script can not install latest Nagios version.${txtrst}
        echo ${txtred}The Script will install Nagios core version 4.3.4${txtrst}

        while true
        do
        echo
        read -p "Are you sure you want to continue? <y/n> " answer
        case $answer in
                [yY]* ) echo "${txtpur}Okay, We will download Nagios version 4.3.4${txtrst}"
                        sleep 2
                                                echo
                        wget  --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.4.tar.gz
                        count=`ls -1 nagios-*.tar.gz  2>/dev/null | wc -l`
                        if [ $count != 0 ]
                        then
                                 nagioscore_ubuntu_434
                        else
                                 echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                 wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-4.3.4.tar.gz
                                 echo "${txtgrn}Nagios version 4.3.4 has been download and we will install it to your server${txtrst}"
                                 sleep 1
                                 nagioscore_ubuntu_434
                        fi
                break;;

                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                        stop
                break;;
                * )     echo "Just enter Y or N, please.";;
        esac
done
fi


############ Search latest version of Nagios Plugin  ################
echo
echo "${txtylw}We will install Nagios plugin.${txtrst}"
echo "${txtylw}Search latest version of Nagios Plugin.${txtrst}"
echo "${txtylw}Please wait a minute ...${txtrst}"
sleep 1
wget -q --no-check-certificate https://nagios-plugins.org/download/
sed  '/.sha1/d' index.html 2> /dev/null | awk '{print $7}' |  sort | cut -c15-25 > nagplug.txt
echo
year=`date +%Y`
echo
cat nagplug.txt | grep $year > latest_year.txt 2> /dev/null

for i in `cat latest_year.txt`
do
date -d $i '+%d-%m-%Y' 2> /dev/null
done > result.txt

cat result.txt | sort -t"-" -k2,2 | sed -n '$p' > latest_plugin.txt 2> /dev/null
sed  '/.sha1/d' index.html > index_latest.html 2> /dev/null
version=`cat latest_plugin.txt `
echo $version | awk '{split($1,a,"-");$1=a[3]"-"a[2]"-"a[1]}1' > reverse.txt
reverse=`cat reverse.txt`
date -d "$reverse" +"%Y-%b-%d" > plugin.txt
plugin=`cat plugin.txt`
echo "$plugin" | awk '{split($1,a,"-");$1=a[3]"-"a[2]"-"a[1]}1' > plugin1.txt
plugin1=`cat plugin1.txt`

latest_plugin=`cat index_latest.html | grep $plugin1 | awk '{print $6}' | sed 's/<.*//' | sed 's/.*>//'` 2> /dev/null
#echo $latest_plugin
sleep 3

############ if fails to find the last version, download version 2.2.1 ################
nagios_plugin=`echo $latest_plugin`
folder_plugin=`echo $nagios_plugin | sed -e 's/.tar.gz//g'` 2> /dev/null
wget --no-check-certificate https://nagios-plugins.org/download/$nagios_plugin 2> /dev/null
echo

count=`ls -1 nagios-plugin*.tar.gz  2>/dev/null | wc -l`
if [ $count != 0 ]
then
        echo "${txtcyn}Latest version of Nagios plugin is `echo $latest_plugin` version${txtrst}"
        sleep 3
        echo
        nagiosplugin_ubuntu_update

else
        echo ${txtred}The script detects there is problem when the script search latest Nagios plugin version${txtrst}
        echo ${txtred}So, the script can not install latest Nagios plugin version${txtrst}
        echo ${txtred}The Script will install Nagios plugin version 2.2.1${txtrst}

        while true
        do
        echo
        read -p "Are you sure you want to continue? <y/n> " answer
        case $answer in
                [yY]* ) echo "${txtpur}Okay, We will install Nagios plugin version 2.2.1${txtrst}"
                        sleep 2
                        wget --no-check-certificate https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
                        count=`ls -1 nagios-plugin*.tar.gz  2>/dev/null | wc -l`
                        if [ $count != 0 ]
                        then
                                echo "${txtylw}We will install Nagios plugin version 2.2.1 to your server${txtrst}"
                                sleep 2
                                nagiosplugin_221
                        else
                                echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-plugins-2.2.1.tar.gz
                                echo "${txtgrn}Nagios Plugin version 2.2.1 has been download and we will install it to your server${txtrst}"
                                sleep 2
                                nagiosplugin_221
                        fi
                break;;

                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                        stop
                break;;
                * )     echo "Just enter Y or N, please.";;
        esac
done
fi
}

install_centos() {
############ Installation Some Packages Needed to Install Nagios  ################

echo "${txtylw}Installation Some Packages Needed to Install Nagios.${txtrst}"
rpm -qa| grep php > php.txt
list=`ls -l | grep php.txt | awk '{print $5}'`
#echo $list

if [ $list == 0 ]
then
sleep 1
yum install -y httpd php gcc glibc glibc-common gd gd-devel make net-snmp-* wget zip unzip mailx bind-utils autoconf automake openssl-devel
else
sleep 1
yum install -y httpd gcc glibc glibc-common gd gd-devel make net-snmp-* wget zip unzip mailx bind-utils autoconf automake openssl-devel
fi
echo ""
nagios_centos
}

nagios_centos() {
############ Search latest version of Nagios Core  ################

echo "${txtylw}Search latest version of Nagios Core${txtrst}"
echo "${txtylw}Please wait a minute ...${txtrst}"
sleep 2
echo
wget --no-check-certificate -q https://www.nagios.org/downloads/nagios-core/thanks/?t=1504034794


############ if fails to find the last version, download version 4.3.4 ################
latest_nagios=`cat index.html* 2>/dev/null  | grep "assets.nagios.com/downloads/nagioscore/releases" | sed -n '1p' | awk '{print $2}' | sed -e 's/<[^()]*>//g' | sed -e 's/\/[^()]*\///g' | sed -e 's/\"[^()]*\">//g' | sed -e 's/href=//g' `

count=`ls -1 index.html* 2>/dev/null | wc -l `
if [ $count != 0 ]
then
        echo "${txtcyn}Latest version of Nagios core is `echo $latest_nagios` version${txtrst}"
        sleep 3
        rm -rf index.html*
        echo
        nagios_core=`echo $latest_nagios`
        folder_nagios=`echo $nagios_core | sed -e 's/.tar.gz//g'`
        echo "Downloading Nagios-Latest-version" >> $log
        sleep 2
        wget  --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/$nagios_core
        count=`ls -1 nagios* 2>/dev/null | wc -l `
        if [ $count != 0 ]
                then
                        nagioscore_centos_update
                else
                         echo ${txtred}The script detects there is a problem when download latest Nagios core version.${txtrst}
                         echo ${txtred}So, the script can not install latest Nagios version.${txtrst}
                         echo ${txtred}The Script will install Nagios core version 4.3.4${txtrst}

                         while true
                         do
                         echo
                         read -p "Are you sure you want to continue? <y/n> " answer
                         case $answer in
                                [yY]* ) echo "${txtpur}Okay, We will download Nagios version 4.3.4${txtrst}"
                                        sleep 2
                                                                                wget --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.4.tar.gz
                                        count=`ls -1 nagios-* 2>/dev/null | wc -l `
                                        if [ $count != 0 ]
                                                then
                                                        nagioscore_centos_434
                                                else
                                                        echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                                        wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-4.3.4.tar.gz
                                                        echo "${txtgrn}Nagios version 4.3.4 has been download and we will install it to your server${txtrst}"
                                                        sleep 1
                                                        nagioscore_centos_434
                                                fi
                                break;;
                                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                                        stop
                                break;;
                         * )     echo "Just enter Y or N, please.";;
                         esac
                         done
        fi
else
        echo ${txtred}The script detects there is a problem when the script search latest Nagios core version.${txtrst}
        echo ${txtred}So, the script can not install latest Nagios version.${txtrst}
        echo ${txtred}The Script will install Nagios core version 4.3.4${txtrst}

        while true
        do
        echo
        read -p "Are you sure you want to continue? <y/n> " answer
        case $answer in
                [yY]* ) echo "${txtpur}Okay, We will download Nagios version 4.3.4${txtrst}"
                        sleep 2
                                                echo
                        wget  --no-check-certificate https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.4.tar.gz
                        count=`ls -1 nagios-*.tar.gz  2>/dev/null | wc -l`
                        if [ $count != 0 ]
                        then
                                 nagioscore_centos_434
                        else
                                 echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                 wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-4.3.4.tar.gz
                                 echo "${txtgrn}Nagios version 4.3.4 has been download and we will install it to your server${txtrst}"
                                 sleep 1
                                 nagioscore_centos_434
                        fi
                break;;

                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                        stop
                break;;
                * )     echo "Just enter Y or N, please.";;
        esac
done
fi


############ Search latest version of Nagios Plugin  ################
echo
echo "${txtylw}We will install Nagios plugin.${txtrst}"
echo "${txtylw}Search latest version of Nagios Plugin.${txtrst}"
echo "${txtylw}Please wait a minute ...${txtrst}"
sleep 1
wget -q --no-check-certificate https://nagios-plugins.org/download/
sed  '/.sha1/d' index.html 2> /dev/null | awk '{print $7}' |  sort | cut -c15-25 > nagplug.txt
echo
year=`date +%Y`
echo
cat nagplug.txt | grep $year > latest_year.txt 2> /dev/null

for i in `cat latest_year.txt`
do
date -d $i '+%d-%m-%Y' 2> /dev/null
done > result.txt

cat result.txt | sort -t"-" -k2,2 | sed -n '$p' > latest_plugin.txt 2> /dev/null
sed  '/.sha1/d' index.html > index_latest.html 2> /dev/null
version=`cat latest_plugin.txt `
echo $version | awk '{split($1,a,"-");$1=a[3]"-"a[2]"-"a[1]}1' > reverse.txt
reverse=`cat reverse.txt`
date -d "$reverse" +"%Y-%b-%d" > plugin.txt
plugin=`cat plugin.txt`
echo "$plugin" | awk '{split($1,a,"-");$1=a[3]"-"a[2]"-"a[1]}1' > plugin1.txt
plugin1=`cat plugin1.txt`

latest_plugin=`cat index_latest.html | grep $plugin1 | awk '{print $6}' | sed 's/<.*//' | sed 's/.*>//'` 2> /dev/null
#echo $latest_plugin
sleep 3

############ if fails to find the last version, download version 2.2.1 ################
nagios_plugin=`echo $latest_plugin`
folder_plugin=`echo $nagios_plugin | sed -e 's/.tar.gz//g'` 2> /dev/null
wget --no-check-certificate https://nagios-plugins.org/download/$nagios_plugin 2> /dev/null
echo

count=`ls -1 nagios-plugin*.tar.gz  2>/dev/null | wc -l`
if [ $count != 0 ]
then
        echo "${txtcyn}Latest version of Nagios plugin is `echo $latest_plugin` version${txtrst}"
        sleep 3
        echo
        nagiosplugin_centos_update

else
        echo ${txtred}The script detects there is problem when the script search latest Nagios plugin version${txtrst}
        echo ${txtred}So, the script can not install latest Nagios plugin version${txtrst}
        echo ${txtred}The Script will install Nagios plugin version 2.2.1${txtrst}

        while true
        do
        echo
        read -p "Are you sure you want to continue? <y/n> " answer
        case $answer in
                [yY]* ) echo "${txtpur}Okay, We will install Nagios plugin version 2.2.1${txtrst}"
                        sleep 2
                        wget --no-check-certificate https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
                        count=`ls -1 nagios-plugin*.tar.gz  2>/dev/null | wc -l`
                        if [ $count != 0 ]
                        then
                                echo "${txtylw}We will install Nagios plugin version 2.2.1 to your server${txtrst}"
                                sleep 2
                                nagiosplugin_221
                        else
                                echo "${txtylw}Wait a minute, we will look for another source ...${txtrst}"
                                wget -q http://www.ngoprek.biz/wp-content/uploads/2017/10/nagios-plugins-2.2.1.tar.gz
                                echo "${txtgrn}Nagios Plugin version 2.2.1 has been download and we will install it to your server${txtrst}"
                                sleep 2
                                nagiosplugin_221
                        fi
                break;;

                [nN]* ) echo "${txtred}I am sorry, The script can not fulfill your request${txtrst}"
                        stop
                break;;
                * )     echo "Just enter Y or N, please.";;
        esac
done
fi
}



############ Disable Firewall and SELinux  ################
echo "${txtbld}Please Make Sure that Firewall and SELinux have been disabled so that the installation runs smoothly${txtrst}"
sleep 3
echo
echo

############ Checking Internet  ################
echo "${txtylw}The script will check whether the server is connected to the internet or not. ${txtrst}"
echo "${txtylw}Please wait a moment ... !${txtrst}"
ping -q -c5 google.com >> /dev/null
if [ $? = 0 ]
then
echo "${txtgrn}Great, Your server is connected to the internet${txtrst}"
echo "Your System is Connected to Internet" >> $log
sleep 2
echo
else
echo "${txtred}Your server is not connected to the internet so the script can not continue to install Nagios.${txtrst}"
echo "${txtred}Please connect first ...!${txtrst}"
echo "Please connect to the internet ..." >> $log
stop
fi

############ Checking username  ################
echo "${txtylw}Trying to guess your username "
sleep 2
user=`whoami`
if [ $user = "root" ]
then
echo "${txtgrn}Good, your username is root${txtrst}"
echo "Your username is root" >> $log
echo
else
echo "${txtred}Please change first to root${txtrst}"
echo "Please change first to root" >> $log
stop
fi
sleep 1


############ Checking OS  ################
echo "${txtylw}Trying to guess your operating system "
sleep 2
if [ -f /etc/debian_version ]
then
echo "${txtgrn}Your Operating System is `cat /etc/os-release | grep ^NAME | awk 'NR > 1 {print $1}' RS='"' FS='"'` `cat /etc/debian_version`${txtrst}"
sleep 2
echo
install_ubuntu
echo
else

if [ -f /etc/redhat-release ]
then
#echo "${txtgrn}Your Operating System is `cat /etc/os-release | grep ^NAME | awk 'NR > 1 {print $1}' RS='"' FS='"'`${txtrst}"
echo "${txtgrn}Your Operating System is `cat /etc/redhat-release`"
sleep 2
echo
install_centos
else

if [ -f /etc/SuSE-release ]
then
echo "${txtgrn}Your Operating System is `cat /etc/os-release | grep ^NAME | awk 'NR > 1 {print $1}' RS='"' FS='"'`${txtrst}"
echo
sleep 2
install_suse
echo
else

echo "${txtred}I think your OS is not Debian/Ubuntu or Centos or openSUSE${txtrst}"
echo "${txtred}I am sorry, The script only running in Debian/Ubuntu, Centos, and openSUSE${txtrst}"
echo "${txtred}So, we can not install Nagios in your server${txtrst}"
stop
fi
fi
fi

